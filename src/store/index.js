import { createStore } from 'vuex'

export default createStore({
  state: {
    todos: [
      {
        id: 1,
        title: "un",
        hours: 1,
        responsable: "Responsable 1",
        state: 0
      },
      {
        id: 2,
        title: "deux",
        hours: 1,
        responsable: "Responsable 2",
        state: 0
      },
      {
        id: 3,
        title: "trois",
        hours: 1,
        responsable: "Responsable 3",
        state: 0
      }
    ]
  },
  getters: {
    todosFiltered: (state) =>( {filtreResponsable, filtreEtat}) => {
      let array = filtreResponsable ? state.todos.filter(t => {
        return t.responsable === filtreResponsable
      }) : state.todos
      array = filtreEtat ? array.filter(t => {
        return t.state === filtreEtat
      }) : array
      return array;
    }
  },
  mutations: {
    addTodo(state, todo){
      state.todos.unshift(todo)
    },
    deleteTodo(state, id){
      state.todos = state.todos.filter(t => t.id !== id)
    },
    deleteTodos(state, checkboxes) {
      state.todos = state.todos.filter(t => !checkboxes.has(t.id));
    },
    updateTodo(state, todo){
      state.todos.find(t => t.id === todo.id).title = todo.title;
      state.todos.find(t => t.id === todo.id).hours = todo.hours;
      state.todos.find(t => t.id === todo.id).responsable = todo.responsable;
    },
    changeState(state, {stateTodo, id}) {
      state.todos.find((t) => t.id === id).state = stateTodo
    },
  },
  actions: {
    addTodo({commit}, todo){
      commit('addTodo', todo);
    },
    deleteTodo({commit}, id){
      commit('deleteTodo', id);
    },
    deleteTodos({commit}, checkboxes){
      commit('deleteTodos', checkboxes);
    },
    updateTodo({commit}, todo){
      commit('updateTodo', todo);
    },
    changeState({commit}, {stateTodo, id}) {
      commit('changeState', {stateTodo, id});
    },
  },
  modules: {
  }
})
